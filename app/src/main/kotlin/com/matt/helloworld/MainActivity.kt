
package com.matt.helloworld

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

public class MainActivity : AppCompatActivity() {

    private val POWER_UP = "com.wahoofitness.bolt.buttons.power_button.up"

    val br : BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            when (intent.action) {
                POWER_UP -> finishAndRemoveTask()
            }
        }
    }

    override protected fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override protected fun onResume() {
        super.onResume()

        val filter = IntentFilter()
        filter.addAction(POWER_UP)

        registerReceiver(br, filter)
    }

    override protected fun onPause() {
        super.onPause()
        unregisterReceiver(br)
    }
}
